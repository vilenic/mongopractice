import ssl
import pymongo
import csv
import pandas
import bson

from pprint import pprint

# Подключение

user = 'kurs'
password = '12345678'

host_1 = 'rc1a-xj1mmdjmkqmzzsy7.mdb.yandexcloud.net:27018'
host_2 = 'rc1c-weby5dhq3kwmchju.mdb.yandexcloud.net:27018'
hosts = ','.join([host_1, host_2])

replica_set = 'rs01'
auth_src = 'mongo_db1'

# Формируем URL для подключения к облачному MongoDB кластеру
# 'mongodb://kurs:12345678@rc1a-xj1mmdjmkqmzzsy7.mdb.yandexcloud.net:27018,rc1c-weby5dhq3kwmchju.mdb.yandexcloud.net:27018/?replicaSet=rs01&authSource=mongo_db1'
url = f'mongodb://{user}:{password}@{hosts}/?replicaSet={replica_set}&authSource={auth_src}'

# Собираем клиент для подключения
client = pymongo.MongoClient(
    url,
    ssl_ca_certs='/usr/local/share/ca-certificates/Yandex/YandexInternalRootCA.crt',
    ssl_cert_reqs=ssl.CERT_REQUIRED
)

db = client.mongo_db1
# test_collection = db.get_collection(name='test-collection')
stat_collection = db.get_collection(name='stat')


# insert_one

# stat_collection.insert_one({
#     "ott_stat_id": "5e38526b-99e6-11e9-825d-246e965f0395",
#     "ott_stat_session_id": "5e38526a-99e6-11e9-825d-246e965f0395",
#     "ott_stat_operator_id": "57864d673150b71d14b050b0",
#     "ott_stat_operator_name": "PeersTV (Inetra)",
#     "ott_stat_channel_name": "Первый HD (НСК)",
#     "ott_stat_operator_channel_id": "10338245",
#     "ott_stat_vi_channel_id": "585957ea5b157b59d6266526",
#     "ott_stat_user_id": "3b17face-ff13-464e-a65c-1681da3467e1",
#     "ott_stat_vi_user_id": "82b28a6ee9f5cc8a95f9d4b6b06e2827",
#     "ott_stat_mode_view": "1",
#     "ott_stat_app_id": "3b17face-ff13-464e-a65c-1681da3467e1",
#     "ott_stat_device_ip": "95.47.244.25",
#     "ott_stat_device_type": "4",
#     "ott_stat_device_type_name": "stb",
#     "ott_stat_event_duration": "3599",
#     "ott_stat_event_start_local_user": "2019-06-27 18:50:02",
#     "ott_stat_event_start_local_user_date": "2019-06-27",
#     "ott_stat_event_end_local_user": "2019-06-27 19:50:01",
#     "ott_stat_event_start": "2019-06-27 11:50:02",
#     "ott_stat_event_start_date": "2019-06-27",
#     "ott_stat_event_start_day_of_week": "4",
#     "ott_stat_event_start_week_number_in_year": "26",
#     "ott_stat_event_end": "2019-06-27 12:50:01",
#     "ott_stat_event_end_date": "2019-06-27",
#     "ott_stat_geo_continent_code": "EU",
#     "ott_stat_geo_subdivision_1_name": "Новосибирская область",
#     "ott_stat_geo_city_name": "Бердск",
#     "ott_stat_geo_time_zone": "Asia/Novosibirsk",
#     "ott_stat_geo_time_zone_int": "7",
#     "ott_stat_geo_country_name": "Россия",
#     "ott_stat_geo_continent_name": "Европа",
#     "ott_stat_dt_add_row": "2019-06-28 23:50:31",
#     "ott_stat_user_agent": "Peers.TV/6.18.9 Android/4.4.4 stb/Eltex NV501WAC.NV501WAC/NV501WAC.NV501WAC.armeabi-v7a",
#     "ott_stat_stream_type": "1",
#     "test_embedded_document": {"test_attr": "hello", "another_test_attr": "another_hello"},
# })
#
#
# Запрос в базу
# result = stat_collection.find({})
# rows = list(result)
# pprint(rows[:2])  # Выводит в консоль две записи
#
#
# С простым фильтром
# SELECT * FROM test-collection WHERE ott_stat_operator_name='SPB TV'
# result = stat_collection.find({'ott_stat_operator_name': 'SPB TV'})
# rows = list(result)
# pprint(rows[:2])  # Выводит в консоль две записи
#
#
# # С условием вхождения в массив
# result = test_collection.find(
#     {'ott_stat_operator_name': {'$in': ['SPB TV', 'PeersTV (Inetra)']}}
# )
# rows = list(result)
# pprint(rows[:2])
#
# # Вложенные параметры фильтра
# result = test_collection.find(
#     {
#         'ott_stat_operator_name': 'PeersTV (Inetra)',
#         'ott_stat_geo_time_zone_int': {'$gt': '3', '$lt': '7'}  # Москва - Красноярск
#     }
# )
# rows = list(result)
# print(rows[:2])
# # $eq, $ne, $gt, $gte, $lt, $lte, $in, $nin
#
#
# Проверка на наличие атрибута
# result = stat_collection.find(
#     {
#         'ott_stat_operator_name': 'PeersTV (Inetra)',
#         'ott_stat_geo_time_zone_int': {'$exists': True, '$gt': '3', '$lt': '7'}
#     }
# )
# rows = list(result)
# print(rows[:2])
#
#
# # И + ИЛИ (AND + OR)
# перечисленные через запятую атрибуты в запросе -> AND
# result = stat_collection.find(
#     {
#         'ott_stat_operator_name': 'PeersTV (Inetra)',
#         'ott_stat_geo_time_zone_int': {'$gt': '3', '$lt': '7'},
#         '$or': {
#             'ott_stat_operator_name': 'SPB TV',
#             'ott_stat_geo_time_zone_int': {'$gt': '0', '$lt': '3'}
#         },
#     }
# )
# rows = list(result)
# print(rows[:2])
#
#
# # Поиск по вложенным документам
# result = stat_collection.find_one({"test_embedded_document": {'$exists': True}}) rows = list(result)
# print(rows)
#
# Поиск по атрибутам вложенных документов (точечная нотация: атрибут.вложенный атрибут)
# result = stat_collection.find_one(
#     {
#         "test_embedded_document": {'$exists': True},
#         "test_embedded_document.another_test_attr": "another_hello"
#     }
# )
# rows = list(result)
# print(rows)
#
#
# Сортировка результата по полю по возрастанию
# result = stat_collection.find({'ott_stat_operator_name': 'SPB TV'})
# result.sort('ott_stat_channel_name', pymongo.ASCENDING)
# rows = list(result)
# print(rows[:1000])
#
#
# Список атрибутов, которые хотим извлечь
# query = {'ott_stat_operator_name': 'SPB TV'}
# projection = {
#     'ott_stat_operator_name': 1,
#     'ott_stat_channel_name': 1,
#     'ott_stat_event_duration': 1,
# }
#
# result = stat_collection.find(query, projection)
# rows = list(result)
# print(rows[:10])
#
# # Прикладное использование pandas. Раскомментируем предыдущий блок.
# data = [
#     {
#         'channel': item['ott_stat_channel_name'],
#         'duration': item['ott_stat_event_duration']
#     }
#     for item in rows
# ]
#
# df = pandas.DataFrame(data)
# with pandas.option_context('display.max_rows', None):
#     print(df)


# Update
# $set - установить значение атрибута
# $unset - убрать атрибут
# $rename - переименовать атрибут
# $currentDate - установить значение = текущая дата/время
# $inc - инкрементировать значение с указанным шагом
# $min - меняем значение только если исходное больше
# $max - меняем значение только если исходное меньше

# {
#   <update operator>: { <field1>: <value1>, ... },
#   <update operator>: { <field2>: <value2>, ... },
#   ...
# }

# result = stat_collection.update_one(
#     {'ott_stat_operator_name': 'PeersTV (Inetra)'},
#     {
#         '$set': {'ott_stat_event_duration': '3001'}
#     }
# )
# print(result.modified_count)
#
# result = stat_collection.find_one(
#     {'ott_stat_operator_name': 'PeersTV (Inetra)'},
# )
# print(result.get('ott_stat_event_duration'))

# result = stat_collection.update_many(
#     {'ott_stat_geo_time_zone_int': '3'},
#     {'$set': {"ott_stat_geo_continent_code": "EU"}}
# )

# Заена документа, обращаемся к документу по id
# result = test_collection.replace_one(
#         {'_id': bson.objectid.ObjectId('5f70c42191882ad902e31812')},
#         {
#                 "ott_stat_id": "5e38526b-99e6-11e9-825d-246e965f0395",
#                 "ott_stat_session_id": "5e38526a-99e6-11e9-825d-246e965f0395",
#                 "ott_stat_operator_id": "57864d673150b71d14b050b0",
#                 "ott_stat_operator_name": "PeersTV (Inetra)",
#                 "ott_stat_channel_name": "Первый HD (НСК)",
#                 "ott_stat_operator_channel_id": "10338245",
#         }
# )

# Delete

# result = stat_collection.delete_one(
#     {'ott_stat_operator_name': 'SPB TV'}
# )
# print(result.deleted_count)
#

# result = stat_collection.delete_many(
#     {'ott_stat_operator_name': 'SPB TV', 'ott_stat_device_type_name': 'tablet'}
# )
# print(result.deleted_count)


# Аггрегация

# $match - Операция фильтрации, которая может уменьшить количество документов,
# которые передаются для ввода в следующий этап.

# $group - Непосредственно, сама агрегация

# $sort - Сортирует документы

# $skip - С помощью данной команды мы можем проигнорировать список
# документов в имеющемся множестве.

# $limit - Ограничивает количество документов для вывода на количество,
# переданное методу, начиная, с текущей позиции.

# Просмотры по каналам
# match = {
#     'ott_stat_event_start_date': {'$gte': '2019-06-01', '$lt': '2019-07-01'}
# }
#
# group = {
#     '_id': '$ott_stat_vi_channel_id',
#     'channel_name': {'$first': '$ott_stat_channel_name'},
#     'count': {'$sum': 1}}
#
# pipeline = [{'$match': match}, {'$group': group}]
#
# result = stat_collection.aggregate(pipeline)
# pprint(list(result))
#
# # Уникальные просмотры
# match = {
#     'ott_stat_event_start_date': {'$gte': '2019-06-01', '$lt': '2019-07-01'}
# }
# group1 = {'_id': '$ott_stat_vi_user_id'}
# group2 = {'_id': 1, 'count': {'$sum': 1}}
# pipeline = [{'$match': match}, {'$group': group1}, {'$group': group2}]
# result = stat_collection.aggregate(pipeline)
# pprint(list(result))
#
